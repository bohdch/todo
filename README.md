# Todo List API

This is a simple Todo List RESTful API built with ASP.NET Core 6, Entity Framework Core and MS SQL Server, which allows users to create, read, update, and delete todo items.

## Features

- Create todo items
- Read todo items (get all or get by ID)
- Update todo items
- Delete todo items

## Prerequisites

- .NET 6 SDK: Install the latest version from [here](https://dotnet.microsoft.com/download/dotnet/6.0)
- MS SQL Server: For development, you can install [SQL Server Express](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) or use SQL Server LocalDB

## Setup

1. Clone the repository.

```
git clone https://github.com/yourusername/yourrepository.git
cd yourrepository
```

2. Update the connection string in the `appsettings.json` file to point to your SQL Server instance. You may need to create a new database and set up the user credentials.

```json
"ConnectionStrings": {
  "DefaultConnection": "Server=(localdb)\\mssqllocaldb;Database=TodoDb;Trusted_Connection=True;MultipleActiveResultSets=true"
},
```

3. Build and run the API.

```
dotnet build
dotnet run
```

Now, the API should be running on `http://localhost:5000`.

## Testing

You can use the built-in Swagger UI or any REST client like Postman to test the API. Here are the endpoints:

1. Get all TodoItems:

```
HTTP Verb: GET
URL: http://localhost:5000/api/todo
```

2. Get a specific TodoItem by ID:

```
HTTP Verb: GET
URL: http://localhost:5000/api/todo/{id}
```

3. Create a new TodoItem:

```
HTTP Verb: POST
URL: http://localhost:5000/api/todo
Content-Type: application/json

Body:
{
    "title": "Sample Todo",
    "description": "Sample Todo Description",
    "isDone": false
}
```

4. Update an existing TodoItem:

```
HTTP Verb: PUT
URL: http://localhost:5000/api/todo/{id}
Content-Type: application/json

Body:
{
    "id": {id},
    "title": "Updated Todo",
    "description": "Updated Todo Description",
    "isDone": false
}
```

5. Delete a TodoItem:

```
HTTP Verb: DELETE
URL: http://localhost:5000/api/todo/{id}
```

Replace `{id}` in the above URLs with the ID of the TodoItem you want to operate on.

## Contributing

Feel free to submit issues and enhancement requests, as well as contribute to the project by creating pull requests.

## License

This project is licensed under the MIT License.

## Feedback 

- Was it easy to complete the task using AI? 

Yes

- How long did task take you to complete? (Please be honest, we need it to gather anonymized statistics)

It took 45 minutes

- Was the code ready to run after generation? What did you have to change to make it usable?

The code wasn't ready to run; there were errors that needed to be addressed, such as including a namespace and replacing a Startup file.

- Which challenges did you face during completion of the task?

ChatGPT used the Startup file, which is not commonly used nowadays.

- Which specific prompts you learned as a good practice to complete the task?

Providing feedback to the model in order to get a better result in the future.
