using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TodoApi.Models;
using TodoApi.Data;
using TodoApi.Controllers;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace TodoApi.Tests
{
    public class TodoControllerTests
    {
        private TodoController CreateController()
        {
            // Create a DbContext with InMemory database for testing
            var options = new DbContextOptionsBuilder<TodoContext>()
                .UseInMemoryDatabase("TestDatabase")
                .Options;
            var todoContext = new TodoContext(options);
            return new TodoController(todoContext);
        }

        [Fact]
        public async Task GetTodoItemsReturnsItems()
        {
            // Arrange
            var controller = CreateController();
            controller.PostTodoItem(new TodoItem { Title = "Test item 1", Description = "Test item 1 description" });
            controller.PostTodoItem(new TodoItem { Title = "Test item 2", Description = "Test item 2 description" });

            // Act
            var results = await controller.GetTodoItems();

            // Assert
            var items = Assert.IsAssignableFrom<IEnumerable<TodoItem>>(results.Value);
            Assert.Equal(2, items.Count());
        }

        [Fact]
        public async Task GetTodoItemReturnsNotFoundForInvalidId()
        {
            // Arrange
            var controller = CreateController();

            // Act
            var result = await controller.GetTodoItem(1);

            // Assert
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetTodoItems_ReturnsAllItems()
        {
            // Arrange
            var controller = CreateController();
            await controller.PostTodoItem(new TodoItem { Title = "Test item 1", Description = "Test item 1 description" });
            await controller.PostTodoItem(new TodoItem { Title = "Test item 2", Description = "Test item 2 description" });

            // Act
            var result = await controller.GetTodoItems();

            // Assert
            var actionResult = Assert.IsType<ActionResult<IEnumerable<TodoItem>>>(result);
            var items = Assert.IsAssignableFrom<IEnumerable<TodoItem>>(actionResult.Value);

            // Ensure that at least two items are returned
            Assert.True(items.Count() >= 2);
        }

        [Fact]
        public async Task PostTodoItemReturnsCreatedAtActionResult()
        {
            // Arrange
            var controller = CreateController();
            var todoItem = new TodoItem
            {
                Title = "Test",
                Description = "Test item",
                IsDone = false
            };

            // Act
            var result = await controller.PostTodoItem(todoItem);

            // Assert
            var createdAtActionResult = Assert.IsType<CreatedAtActionResult>(result.Result);
            var createdTodoItem = Assert.IsType<TodoItem>(createdAtActionResult.Value);
        }

        // Create sample TodoItem method
        private async Task<TodoItem> CreateSampleTodoItemAsync(TodoController controller, string title)
        {
            var result = await controller.PostTodoItem(new TodoItem { Title = title, Description = "Sample Description", IsDone = false });
            return (result.Result as CreatedAtActionResult).Value as TodoItem;
        }

        [Fact]
        public async Task PutTodoItemReturnsBadRequestForMismatchedIds()
        {
            // Arrange
            var controller = CreateController();
            var todoItem = await CreateSampleTodoItemAsync(controller, "Test");

            // Act
            var result = await controller.PutTodoItem(2, todoItem);

            // Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task PutTodoItemUpdatesTodoItem()
        {
            // Arrange
            var controller = CreateController();
            var todoItem = await CreateSampleTodoItemAsync(controller, "Test");

            todoItem.Title = "Updated";

            // Act
            var result = await controller.PutTodoItem(todoItem.Id, todoItem);

            // Assert
            Assert.IsType<NoContentResult>(result);

            var updatedItem = await controller.GetTodoItem(todoItem.Id);
            Assert.Equal("Updated", updatedItem.Value.Title);
        }

        [Fact]
        public async Task DeleteTodoItemReturnsNotFoundForInvalidId()
        {
            // Arrange
            var controller = CreateController();

            // Act
            var result = await controller.DeleteTodoItem(1);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task DeleteTodoItemDeletesItem()
        {
            // Arrange
            var controller = CreateController();
            var todoItem = await CreateSampleTodoItemAsync(controller, "Test");

            // Act
            var result = await controller.DeleteTodoItem(todoItem.Id);

            // Assert
            Assert.IsType<NoContentResult>(result);

            var notFoundResult = await controller.GetTodoItem(todoItem.Id);
            Assert.IsType<NotFoundResult>(notFoundResult.Result);
        }
    }
}